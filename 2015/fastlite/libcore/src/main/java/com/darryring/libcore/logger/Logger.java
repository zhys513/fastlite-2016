package com.darryring.libcore.logger;


/**
 * Created by hljdrl on 15/12/11.
 */
public interface Logger  {

    /**
     * @param tag
     * @param msg
     */
    public void i(String tag,String msg);

    /**
     * @param tag
     * @param msg
     */
    public void d(String tag,String msg);

    /**
     * @param tag
     * @param msg
     */
    public void v(String tag,String msg);

    /**
     * @param tag
     * @param msg
     */
    public void e(String tag,String msg);

    /**
     * @param tag
     * @param msg
     */
    public void e(String tag,Throwable msg);
}
