package com.darryring.fast.util;

import com.darryring.fast.DarryApplication;

public class MoviceSize {
	
	
	public int width;
	public int height;
	
	
	public static MoviceSize makeHeight(float moviceWidth,final float moviceHeight){
		MoviceSize _size = new MoviceSize();
//		float sw = 1080;
		float sh =1812 ;
		DarryApplication _app = DarryApplication.getInstance();
		float shx = _app.getScreenHeight()/sh;
		float _moviceW = moviceWidth*shx;
		float _moviceH = moviceHeight*shx;
		_size.width =(int) _moviceW;
		_size.height = (int)_moviceH;
		return _size;
	}
	public static MoviceSize make( float moviceWidth,final float moviceHeight){
		if(moviceWidth==moviceHeight){
			return makeHeight(moviceWidth, moviceHeight);
		}
		MoviceSize _size = new MoviceSize();
		float sw = 1080;
		float sh = 1812 ;
		DarryApplication _app = DarryApplication.getInstance();
		float swx = _app.getScreenWidth()/sw;
		float shx = _app.getScreenHeight()/sh;
		float _moviceW = moviceWidth*swx;
		float _moviceH = moviceHeight*shx;
		_size.width =(int) _moviceW;
		_size.height = (int)_moviceH;
		return _size;
	}
	public static int makeText( float moviceWidth,final float moviceHeight){
		float sw = 1080;
		float sh = 1812 ;
		DarryApplication _app = DarryApplication.getInstance();
		float swx = _app.getScreenWidth()/sw;
		float shx = _app.getScreenHeight()/sh;
		float _moviceW = moviceWidth*swx;
		float _moviceH = moviceHeight*shx;
		return(int) (_moviceW+_moviceH)/2;
	}
	public static MoviceSize make(final float screenWidth,final float screenHeight, float moviceWidth,
		   final float moviceHeight){
		if(moviceWidth==moviceHeight){
			return makeHeight(moviceWidth, moviceHeight);
		}
		MoviceSize _size = new MoviceSize();
		float sw = 1080;
		float sh = 1812 ;
		float swx = screenWidth/sw;
		float shx = screenHeight/sh;
		float _moviceW = moviceWidth*swx;
		float _moviceH = moviceHeight*shx;
		_size.width =(int) _moviceW;
		_size.height = (int)_moviceH;
		return _size;
	}
	public static MoviceSize makeZoomIn(final float screenWidth,final float screenHeight, float moviceWidth,
			final float moviceHeight){
		if(moviceWidth==moviceHeight){
			return makeHeight(moviceWidth, moviceHeight);
		}
		MoviceSize _size = new MoviceSize();
		float sw = 1080;
		float sh = 1812 ;
		float swx = screenWidth/sw;
		float shx = screenHeight/sh;
		float _moviceW = moviceWidth*swx;
		float _moviceH = moviceHeight*shx;
		int _width =(int) _moviceW;
		int _height = (int)_moviceH;
		if(_width%2!=0){
			_width = (_width-1);
		}
		if(_height%2!=0){
			_height = (_height-1);
		}
		_size.width = _width;
		_size.height = _height;
		return _size;
	}

}
