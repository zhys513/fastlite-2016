package com.darryring.fast.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.darryring.fast.R;
import com.darryring.libmodel.entity.ChatID;
import com.darryring.libmodel.entity.FSession;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hljdrl on 16/5/28.
 */
public class SessionAdapter extends ArrayAdapter<FSession> {
    static Map<String,Integer> icons = new HashMap<>();
    static{
        icons.put(ChatID.CHAT_ID_SYSTEM_100,R.drawable.ic_icon_chatid_100);
        icons.put(ChatID.CHAT_ID_SYSTEM_200,R.drawable.ic_icon_chatid_200);
        icons.put(ChatID.CHAT_ID_SYSTEM_300,R.drawable.ic_icon_chatid_300);
        icons.put(ChatID.CHAT_ID_SYSTEM_400,R.drawable.ic_icon_chatid_400);
        icons.put(ChatID.CHAT_ID_SYSTEM_500,R.drawable.ic_icon_chatid_500);
        icons.put(ChatID.CHAT_ID_SYSTEM_600,R.drawable.ic_icon_chatid_600);
        icons.put(ChatID.CHAT_ID_SYSTEM_700,R.drawable.ic_icon_chatid_700);
        icons.put(ChatID.CHAT_ID_SYSTEM_800,R.drawable.ic_icon_chatid_800);
        icons.put(ChatID.CHAT_ID_SYSTEM_900,R.drawable.ic_icon_chatid_900);
    }

    LayoutInflater inflater;
    public SessionAdapter(Context context, List<FSession> objects) {
        super(context, 0, objects);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View _view, ViewGroup parent) {
        ViewHolder _viewHolder = null;
        if(_view==null){
            _view = inflater.inflate(R.layout.item_session,null);
            _viewHolder = new ViewHolder();
            _viewHolder.imageview = (ImageView) _view.findViewById(R.id.image_avator);
            _viewHolder.name = (TextView) _view.findViewById(R.id.tv_ct_name);
            _viewHolder.body = (TextView) _view.findViewById(R.id.tv_ct_sign);
            _view.setTag(_viewHolder);
        }else{
            _viewHolder = (ViewHolder) _view.getTag();
        }
        FSession _ct = getItem(position);
        _viewHolder.name.setText(_ct.getFromId());
        _viewHolder.body.setText(_ct.getMessage());
        int icon = icons.get(_ct.getFromId());
        _viewHolder.imageview.setBackgroundResource(0);
        _viewHolder.imageview.setImageResource(icon);
        return _view;
    }
    private static class ViewHolder{
        public ImageView imageview;
        public TextView  name;
        public TextView  body;
    }
}
