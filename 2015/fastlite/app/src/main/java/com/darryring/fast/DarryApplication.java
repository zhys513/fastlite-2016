package com.darryring.fast;

import android.app.Application;
import android.util.DisplayMetrics;
import android.util.Log;

import com.darryring.fast.activitys.ErrorCrashActivity;
import com.darryring.libcore.util.PinYin;
import com.darryring.libcore.util.SystemUtil;

import cat.ereza.customactivityoncrash.CustomActivityOnCrash;

/**
 * Created by hljdrl on 15/12/11.
 */
public class DarryApplication extends Application {

    private String APP_TAG="FAST-LITE-ANDROID";

    private static DarryApplication mApplication = null;

    public static final DarryApplication getInstance(){
        return mApplication;
    }
    String TAG="DarryApplication";

    private int mScreenHeight;

    private int mScreenWidth;

    public final int getScreenHeight(){
        return mScreenHeight;
    }
    public final int getScreenWidth(){
        return mScreenWidth;
    }
    @Override
    public void onCreate() {
        mApplication = this;
        super.onCreate();
        Log.i(TAG,"onCreate...");
        //
        CustomActivityOnCrash.install(this);
        CustomActivityOnCrash.setErrorActivityClass(ErrorCrashActivity.class);
        //
        if(inMainProcess()){
            PinYin.init(this);
            PinYin.validate();
            DisplayMetrics displayMetrics = new DisplayMetrics();
            displayMetrics = getResources().getDisplayMetrics();
            mScreenHeight = displayMetrics.heightPixels;
            mScreenWidth = displayMetrics.widthPixels;
            Log.i(TAG,"ScreenSize--->"+mScreenWidth+" x "+mScreenHeight);
        }
    }

    public boolean inMainProcess() {
        String packageName = getPackageName();
        String processName = SystemUtil.getProcessName(this);
        return packageName.equals(processName);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Log.i(TAG,"onLowMemory...");
    }
    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        Log.i(TAG,"onLowMemory--->level-->"+level);
    }
    @Override
    public void onTerminate() {
        super.onTerminate();
        Log.i(APP_TAG, "onTerminate()....");
    }

}
