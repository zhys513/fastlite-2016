package com.darryring.libmodel.entity;

/**
 * Created by hljdrl on 16/4/11.
 */
public class FContact {

    private String chatId;
    private String name;

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        if(name!=null&&name.length()>0){
            return name;
        }
        if(chatId!=null&&chatId.length()>0){
            return chatId;
        }
        return super.toString();
    }
}
