package com.darryring.libchat;

/**
 *
 */
public class ChatCode {
    /**
     * 账号在别处登录
     */
    public final static byte ERROR_DISCONNECT_CONFLICT = 0x1;

    /**
     * 用户被删除
     */
    public final static byte ERROR_DISCONNECT_REMOVED = 0x2;

    /**
     * 其他断开连接类型
     */
    public final static byte ERROR_DISCONNECT_OTHER = 0x3;

    /**
     * 消息类型--文本
     */
    public final static byte MESSAGE_TYPE_TEXT = 0x11;
    /**
     * 消息类型--图片
     */
    public final static byte MESSAGE_TYPE_IMG = 0x12;
    /**
     * 消息类型--语音
     */
    public final static byte MESSAGE_TYPE_VOICE = 0x13;


    /**
     * 通讯录类型--添加用户
     */
    public final static byte MANAGER_CONTACT_CODE_ADDCONTACT = 0x20;
    /**
     * 通讯录类型--删除用户
     */
    public final static byte MANAGER_CONTACT_CODE_REMOVECONTACT = 0x21;
    /**
     * 通讯录类型--通过验证
     */
    public final static byte MANAGER_CONTACT_CODE_ACCEPTInvitation = 0x22;
    /**
     * 通讯录类型--拒绝验证
     */
    public final static byte MANAGER_CONTACT_CODE_REEFUSEInvitation = 0x23;

    /**
     * 群组通讯录类型--加载完成群组信息
     */
    public final static byte MANAGER_GROUPCONTACT_CODE_LOADGROUPINFO = 0x24;


    /**
     * 新朋友状态--待验证
     */
    public final static int NEWFRIEND_STATUS_NEWFRIEND_WAIT = 0;
    /**
     * 新朋友状态--等待对方验证
     */
    public final static int NEWFRIEND_STATUS_NEWFRIEND_SIDE_WAIT = 1;
    /**
     * 新朋友状态--通过验证
     */
    public final static int NEWFRIEND_STATUS_NEWFRIEND_PASS = 2;
    /**
     * 新朋友状态--拒绝验证
     */
    public final static int NEWFRIEND_STATUS_NEWFRIEND_REFUSE = 3;
    /**
     * 新朋友状态--对方已认证
     */
    public final static int NEWFRIEND_STATUS_NEWFRIEND_SIDE_PASS = 4;
    /**
     * 新朋友状态--对方拒绝
     */
    public final static int NEWFRIEND_STATUS_NEWFRIEND_SIDE_REFUSE = 5;

    /**
     * 群类型--临时群
     */
    public final static int GROUPCONTACT_TYPE_TEMPROOM = 0;
    /**
     * 群类型--班级群
     */
    public final static int GROUPCONTACT_TYPE_CLASSROOM = 1;

    /**
     * 登录错误码--用户名密码错误
     */
    public final static byte ERRORCODE_LOGIN_NAMEORPWD = 0x40;
    /**
     * 登录错误码--密码错误
     */
    public final static byte ERRORCODE_LOGIN_PASSWORD = 0x41;
    /**
     * 登录错误码--网络异常
     */
    public final static byte ERRORCODE_LOGIN_NETWORK = 0x51;
    /**
     * 登录错误码--其他未知错误
     */
    public final static byte ERRORCODE_LOGIN_OTHER = 0x52;

    
    
}
