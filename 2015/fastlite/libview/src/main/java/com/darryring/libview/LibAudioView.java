package com.darryring.libview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

/**
 * Created by Darry.Ring on 15/12/11.
 */
public class LibAudioView extends FrameLayout {

    String TAG="LibAudioView";

    String VERSION="1.0.0";


    public LibAudioView(Context context) {
        super(context);
    }

    public LibAudioView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LibAudioView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

}
