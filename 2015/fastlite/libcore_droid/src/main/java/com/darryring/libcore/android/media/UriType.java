package com.darryring.libcore.android.media;

/**
 * Created by hljdrl on 15/12/26.
 */
public class UriType {

    public static boolean isAssetsUriType(String uri){
        if(uri.startsWith("assets")){
            return true;
        }
        return false;
    }

    public static boolean isHttpUriType(String uri){
        if(uri.startsWith("http")){
            return true;
        }
        return false;
    }

    public static boolean isFileUriType(String uri){
        if(uri.startsWith("file")){
            return true;
        }
        return false;
    }

    /**
     * @param http
     * @return
     */
    public static String formatHttpUri(String http){
        StringBuffer _buf = new StringBuffer();
        _buf.append(http);
        return _buf.toString();
    }

    /**
     * @param file
     * @return
     */
    public static String formatFileUri(String file){
        StringBuffer _buf = new StringBuffer();
        if(!file.startsWith("file")){
            _buf.append("file://");
        }
        _buf.append(file);
        return _buf.toString();
    }

    /**
     * @param assets
     * @return
     */
    public static String formatAssetsUri(String assets){
        StringBuffer _buf = new StringBuffer();
        if(!assets.startsWith("assets")){
            _buf.append("assets://");
        }
        _buf.append(assets);
        return _buf.toString();
    }
}
